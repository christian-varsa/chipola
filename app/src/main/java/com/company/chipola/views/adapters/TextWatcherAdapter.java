package com.company.chipola.views.adapters;

import android.text.Editable;
import android.text.TextWatcher;

/**
 * Created by christian.vargas on 3/13/18.
 */

public class TextWatcherAdapter implements TextWatcher{

    @Override
    public void afterTextChanged(Editable s) {

    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }
}
