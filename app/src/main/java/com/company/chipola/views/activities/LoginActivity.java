package com.company.chipola.views.activities;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.support.annotation.ColorRes;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.ImageViewTarget;
import com.company.chipola.R;
import com.company.chipola.views.adapters.AuthAdapter;
import com.company.chipola.views.viewPager.AnimatedViewPager;

import java.util.List;

import butterknife.BindViews;
import butterknife.ButterKnife;

public class LoginActivity extends AppCompatActivity {

    @BindViews(value = {R.id.logo, R.id.first, R.id.last})
    protected List<ImageView> sharedElements;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        final AnimatedViewPager pager = ButterKnife.findById(this, R.id.pager);
        final ImageView background = ButterKnife.findById(this, R.id.scrolling_background);
        int[] screenSize = screenSize();

        for (ImageView element : sharedElements) {

            if (element.getId() == R.id.logo){
                @ColorRes int colorLogo = element.getId() == R.id.logo ?  R.color.black_transparent : R.color.white_transparent ;
                DrawableCompat.setTint(element.getDrawable(), ContextCompat.getColor(this, colorLogo));
            }

            if (element.getId() == R.id.first){
                @ColorRes int colorLogo = element.getId() == R.id.first ?  R.color.black_transparent : R.color.white_transparent ;
                DrawableCompat.setTint(element.getDrawable(), ContextCompat.getColor(this, colorLogo));
            }


            if (element.getId() == R.id.last){
                @ColorRes int colorLogo = element.getId() == R.id.last ?  R.color.black_transparent : R.color.white_transparent ;
                DrawableCompat.setTint(element.getDrawable(), ContextCompat.getColor(this, colorLogo));
            }
        }

        //load a very big image and resize it, so it fits our needs
        Glide.with(this)
                .load(R.mipmap.img_login_background)
                .asBitmap()
                .override(screenSize[0] * 2, screenSize[1])
                .diskCacheStrategy(DiskCacheStrategy.RESULT)
                .into(new ImageViewTarget<Bitmap>(background) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        background.setImageBitmap(resource);
                        background.post(() -> {
                            //we need to scroll to the very left edge of the image
                            //fire the scale animation
                            background.scrollTo(-background.getWidth() / 2, 0);
                            ObjectAnimator xAnimator = ObjectAnimator.ofFloat(background, View.SCALE_X, 4f, background.getScaleX());
                            ObjectAnimator yAnimator = ObjectAnimator.ofFloat(background, View.SCALE_Y, 4f, background.getScaleY());
                            AnimatorSet set = new AnimatorSet();
                            set.playTogether(xAnimator, yAnimator);
                            set.setDuration(getResources().getInteger(R.integer.duration));
                            set.start();
                        });
                        pager.post(() -> {
                            AuthAdapter adapter = new AuthAdapter(getSupportFragmentManager(), pager, background, sharedElements);
                            pager.setAdapter(adapter);
                        });
                    }
                });
    }

    private int[] screenSize() {
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return new int[]{size.x, size.y};
    }
}
