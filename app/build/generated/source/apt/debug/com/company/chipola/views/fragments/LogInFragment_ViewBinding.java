// Generated code from Butter Knife. Do not modify!
package com.company.chipola.views.fragments;

import android.support.annotation.UiThread;
import android.view.View;
import android.widget.EditText;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.company.chipola.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class LogInFragment_ViewBinding extends AuthFragment_ViewBinding {
  private LogInFragment target;

  private View view2131558593;

  @UiThread
  public LogInFragment_ViewBinding(final LogInFragment target, View source) {
    super(target, source);

    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.caption, "method 'onLoginClick'");
    view2131558593 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onLoginClick();
      }
    });
    target.views = Utils.listOf(
        Utils.findRequiredViewAsType(source, R.id.fr_login_email_text, "field 'views'", EditText.class), 
        Utils.findRequiredViewAsType(source, R.id.fr_login_password_text, "field 'views'", EditText.class));
  }

  @Override
  public void unbind() {
    LogInFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.views = null;

    view2131558593.setOnClickListener(null);
    view2131558593 = null;

    super.unbind();
  }
}
