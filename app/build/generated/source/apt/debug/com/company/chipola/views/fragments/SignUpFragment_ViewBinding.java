// Generated code from Butter Knife. Do not modify!
package com.company.chipola.views.fragments;

import android.support.annotation.UiThread;
import android.view.View;
import android.widget.EditText;
import butterknife.internal.Utils;
import com.company.chipola.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SignUpFragment_ViewBinding extends AuthFragment_ViewBinding {
  private SignUpFragment target;

  @UiThread
  public SignUpFragment_ViewBinding(SignUpFragment target, View source) {
    super(target, source);

    this.target = target;

    target.views = Utils.listOf(
        Utils.findRequiredViewAsType(source, R.id.fr_sing_up_email_text, "field 'views'", EditText.class), 
        Utils.findRequiredViewAsType(source, R.id.fr_sing_up_name_text, "field 'views'", EditText.class), 
        Utils.findRequiredViewAsType(source, R.id.fr_sing_up_password_text, "field 'views'", EditText.class), 
        Utils.findRequiredViewAsType(source, R.id.fr_sing_up_password_confirm_text, "field 'views'", EditText.class));
  }

  @Override
  public void unbind() {
    SignUpFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.views = null;

    super.unbind();
  }
}
